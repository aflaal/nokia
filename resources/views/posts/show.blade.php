@extends('layouts.app')

@section('content')
<a href="/posts" class="btn btn-primary">GO Back</a>
    <h2 class="mb-3">{{$post->title}}</h2>
    <h3 mb-4>{!!$post->body!!}</h3>
    <a href="/posts/{{$post->id}}/edit" class="btn btn-primary mb-3" >Edit</a>

    {!!Form::open(['action' => ['PostsController@destroy', $post->id], 'method' => 'POST', 'class' => 'float-rigth'])!!}
        {{Form::hidden('_method','DELETE')}}
        {{Form::submit('Delete', ['class' => 'btn btn-danger'])}}
    {!!Form::close()!!}
@endsection