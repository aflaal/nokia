@extends('layouts.app')

@section('content')
    @if(count($posts) > 0)
        @foreach($posts as $post)
            <div>
            <h2 class="mb-3 mt-3"><a href="/posts/{{$post->id}}">{{$post->title}}</a></h2>
            <small>Writtten on:{{$post->created_at}}</small>
            </div>
            <hr>
        @endforeach
        {{$posts->links()}}
    @endif
@endsection