<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
   public function index(){
       $title = 'Nokia Blogger';
       return view('pages.index')->with('title',$title);
   }

   public function about(){
       return view('pages.about');
   }

   public function services(){
       $data = array(
           'title' => 'Our services',
           'services' => ['Webdesign','SEO','Programming']
       );
       return view('pages.services')->with($data);
   }
}
