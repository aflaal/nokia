@extends('layouts.app')

@section('content')
  <div class="jumbotron">
      <h1 class="display-4 text-center">{{$title}}</h1>
      <p class="lead text-justify">ing Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, content here', making it look like readable English. Many desktop publishing packages and web page editors now use Lorem Ipsum as their default model text, and a search for 'lorem ipsum' will uncover many web sites still in their infancy. Various versions .</p>
      <hr class="my-4">
      <p class="text-center">It uses utility classes for typography and spacing to space content out within the larger container.</p>
      <p class="lead text-center">
        <a class="btn btn-primary btn-lg" href="#" role="button">Login</a>
        <a class="btn btn-success btn-lg" href="#" role="button">Signup</a>
      </p>
    </div>
@endsection
